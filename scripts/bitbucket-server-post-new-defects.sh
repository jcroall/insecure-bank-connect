#!/bin/sh

BBS_URL=http://ubuntu-server:7990
COMMIT_ID=`git rev-parse HEAD`
REPORT_KEY=com.synopsys.coverity.report
BBS_PROJECT=IB
BBS_REPO=insecure-bank

CWD=`pwd`
BASEDIR=$(dirname "$0") 
python3 $BASEDIR/bitbucket-server-create-code-insights.py

echo ----------------------------------
echo create report
echo ----------------------------------
curl --verbose \
-H "Content-type: application/json" \
-H "Authorization: Bearer MTU5MTU1NzIyMzU4Oh+a6gzAaBSBkXfOv3DDHq4nRJ4w" \
-X PUT \
-d @synopsys-bitbucket-sast-report.json \
"$BBS_URL/rest/insights/latest/projects/$BBS_PROJECT/repos/$BBS_REPO/commits/$COMMIT_ID/reports/$REPORT_KEY"

echo ----------------------------------
echo Delete old annotations
echo ----------------------------------
# Delete old annotations from the report (they may not exist but it is better to be safe)
curl --verbose \
-H "Authorization: Bearer MTU5MTU1NzIyMzU4Oh+a6gzAaBSBkXfOv3DDHq4nRJ4w" \
-H "X-Atlassian-Token: no-check" \
-X DELETE \
"$BBS_URL/rest/insights/latest/projects/IB/repos/insecure-bank/commits/$COMMIT_ID/reports/$REPORT_KEY/annotations"

echo ----------------------------------
echo Create annotations
echo ----------------------------------
# Create the annotations
curl --verbose \
-H "Content-type: application/json" \
-H "Authorization: Bearer MTU5MTU1NzIyMzU4Oh+a6gzAaBSBkXfOv3DDHq4nRJ4w" \
-X POST \
-d @synopsys-bitbucket-sast-annotations.json \
"$BBS_URL/rest/insights/latest/projects/IB/repos/insecure-bank/commits/$COMMIT_ID/reports/$REPORT_KEY/annotations"

exit 0
