#!/usr/bin/python

import json
import sys
import os
import argparse
import urllib
import glob
import datetime
import requests
import base64

# Parse command line arguments
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Post Coverity issue summary to Bitbucket pipelines Code Insights')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--bb_repo_uuid', help='$BITBUCKET_REPO_UUID - The repo id')
parser.add_argument('--bb_repo_owner', help='$BITBUCKET_REPO_OWNER - The repo owner name')
parser.add_argument('--bb_repo_owner_uuid', help='$BITBUCKET_REPO_OWNER_UUID - The repo owner name as id')
parser.add_argument('--bb_commit_hash', help='$BITBUCKET_COMMIT - The hash of the commit running the scan')
parser.add_argument('--bb_app_password', help='The App Password set for a Bitbucket profile')
parser.add_argument('mergeKeys', nargs=argparse.REMAINDER)
args = parser.parse_args()

debug = int(args.debug)
repo_uuid = str(args.bb_repo_uuid)[1:-1]
repo_owner = str(args.bb_repo_owner)
repo_owner_uuid = str(args.bb_repo_owner_uuid)[1:-1]
commit_hash = str(args.bb_commit_hash)
app_password = str(args.bb_app_password)

mergeKeys = args.mergeKeys

# Populate a map with the merge keys we want
mergeKeysToMatch = dict()
for mergeKey in mergeKeys:
    print("Match Merge Key: " + mergeKey)
    mergeKeysToMatch[mergeKey] = 1

jsonFiles = glob.glob("./coverity-output.json")
jsonFile = jsonFiles[0]

# Process output from Coverity
with open(jsonFile) as f:
  data = json.load(f)

print("Reading incremental analysis results from " + jsonFile)
if(debug): print("DEBUG: " + json.dumps(data, indent = 4, sort_keys=True) + "\n")

# Loop through found issues for specified merge keys, and build out output map
# TODO: Can there be multiple entries for the merge key? I think the right thing would be to list all of them.
sast_report = dict()
sast_report["report_type"] = "SECURITY"
sast_report["title"] = "Synopsys SAST Report"
sast_report["details"] = "Incremental SAST report from Coverity, works with pull requests"
sast_report["result"] = "FAILED"
sast_report["reporter"] = "Coverity"
sast_report["link"] = "https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html"
sast_report["logo_url"] = "https://www.cloudfoundry.org/wp-content/uploads/icon_synopsys@2x.png"

now = datetime.datetime.now()
sast_report["created_on"] = now.strftime('%Y-%m-%dT%H:%M:%SZ')
sast_report["updated_on"] = now.strftime('%Y-%m-%dT%H:%M:%SZ')

annotations = []

for item in data["issues"]:
    checkerName = item["checkerName"]
    checkerProperties = item["checkerProperties"]
    subcategoryShortDescription = checkerProperties["subcategoryShortDescription"]
    subcategoryLongDescription = checkerProperties["subcategoryLongDescription"]
    cwe = checkerProperties["cweCategory"]
    impact = checkerProperties["impact"]
    codeLangauge = item["code-language"]
    mergeKey = item["mergeKey"]
    strippedMainEventFilePathname = item["strippedMainEventFilePathname"]
    mainEventLineNumber = item["mainEventLineNumber"]

    # convert the result json to bitbucket friendly json
    eventNumber = 1
    #if mergeKey in mergeKeysToMatch:
    if 1
      print("Foud merge key " + mergeKey + "\n")
      newIssue = dict()
      newIssue["external_id"] = mergeKey
      newIssue["annotation_type"] = "VULNERABILTIY" #TODO: Change this
      newIssue["summary"] = subcategoryShortDescription
      newIssue["details"] = subcategoryLongDescription
      newIssue["severity"] = impact.upper()
      isLineNumberAvailable = False
    
      count = 0
      for event in item["events"]:
        if event["main"]:
           isLineNumberAvailable = True
           issueWithLine = newIssue.copy()
           issueWithLine["external_id"] = issueWithLine["external_id"] + "_" + str(count)
           issueWithLine["path"] = event["strippedFilePathname"]
           issueWithLine["line"] = event["lineNumber"]
           issueWithLine["details"] = event["eventDescription"]
           count = count + 1
           annotations.append(issueWithLine)

      #print newIssue
      if not isLineNumberAvailable:
        annotations.append(newIssue)

# dump the json
with open('synopsys-bitbucket-sast-report.json', 'w') as fp:
  json.dump(sast_report, fp, indent=4)

with open('synopsys-bitbucket-sast-annotations.json', 'w') as fp:
  json.dump(annotations, fp, indent=4)

print("Sending the POST requests to Bitbucket ...")
# creating a common header for Bitbucket PUT requests
userpass = repo_owner + app_password
auth = base64.b64encode(userpass)
headers = {
    "Content-Type": "application/json",
    "Authorization": auth
}

# sending report to bitbucket ... 
url = "https://api.bitbucket.org/2.0/repositories/%7B"+repo_owner_uuid+"%7D/%7B"+repo_uuid+"%7D/commit/"+commit_hash+"/reports/synopsys-sast-report"
print(url)
report_response = requests.put(url, data=json.dumps(sast_report), headers=headers)
print("Report response: " + report_response.text + "\n")
print("Report posted to Bitbucket ...")

# sending annotations to Bitbucket 1 by 1...
count = 0
for annotation in annotations:
    count = count + 1
    url = "https://api.bitbucket.org/2.0/repositories/%7B"+repo_owner_uuid+"%7D/%7B"+repo_uuid+"%7D/commit/"+commit_hash+"/reports/synopsys-sast-report/annotations/"+str(annotation["external_id"])
    annotations_response = requests.put(url, data=json.dumps(annotation), headers=headers)
    print("Annotations response: " + annotations_response.text + "\n")
print("Annotations posted to Bitbucket ...")
