#!/usr/bin/python

import json
import sys
import os
import argparse
import urllib
import glob
import datetime
import requests
import base64

# Parse command line arguments
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
        description='Post Coverity issue summary to Bitbucket Data Center/Server Code Insights')
parser.add_argument('--debug', default=0, help='set debug level [0-9]')
parser.add_argument('--coverity_url', help='The link to full results in the Coverity server')
parser.add_argument('mergeKeys', nargs=argparse.REMAINDER)
args = parser.parse_args()

debug = int(args.debug)
coverity_url = str(args.coverity_url)
mergeKeys = args.mergeKeys

# Populate a map with the merge keys we want
mergeKeysToMatch = dict()
for mergeKey in mergeKeys:
    print("Match Merge Key: " + mergeKey)
    mergeKeysToMatch[mergeKey] = 1

jsonFiles = glob.glob("./coverity-output.json")
jsonFile = jsonFiles[0]

# Process output from Coverity
with open(jsonFile) as f:
  data = json.load(f)

print("Reading incremental analysis results from " + jsonFile)
if(debug): print("DEBUG: " + json.dumps(data, indent = 4, sort_keys=True) + "\n")

# Loop through found issues for specified merge keys, and build out output map
# TODO: Can there be multiple entries for the merge key? I think the right thing would be to list all of them.
sast_report = dict()
sast_report["report_type"] = "SECURITY"
sast_report["title"] = "Synopsys SAST Report"
sast_report["details"] = "SAST report from Coverity"
sast_report["result"] = "FAIL"
sast_report["reporter"] = "Coverity"
sast_report["link"] = "https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html"
sast_report["logo_url"] = "https://www.cloudfoundry.org/wp-content/uploads/icon_synopsys@2x.png"

now = datetime.datetime.now()
sast_report["created_on"] = now.strftime('%Y-%m-%dT%H:%M:%SZ')
sast_report["updated_on"] = now.strftime('%Y-%m-%dT%H:%M:%SZ')

annotations = []

for item in data["issues"]:
    checkerName = item["checkerName"]
    checkerProperties = item["checkerProperties"]
    subcategoryShortDescription = checkerProperties["subcategoryShortDescription"]
    subcategoryLongDescription = checkerProperties["subcategoryLongDescription"]
    cwe = checkerProperties["cweCategory"]
    impact = checkerProperties["impact"]
    codeLangauge = item["code-language"]
    mergeKey = item["mergeKey"]
    strippedMainEventFilePathname = item["strippedMainEventFilePathname"]
    mainEventLineNumber = item["mainEventLineNumber"]

    # convert the result json to bitbucket friendly json
    eventNumber = 1
    # Hard-coded to export all findings, not just matching merge keys
    #if mergeKey in mergeKeysToMatch:
    if 1:
      #print("Foud merge key " + mergeKey + "\n")
      newIssue = dict()
      #newIssue["external_id"] = mergeKey
      #newIssue["annotation_type"] = "VULNERABILTIY" #TODO: Change this
      #newIssue["message"] = subcategoryShortDescription + ": " + subcategoryLongDescription
      newIssue["message"] = subcategoryShortDescription
      newIssue["severity"] = impact.upper()
      isLineNumberAvailable = False
    
      count = 0
      for event in item["events"]:
        if event["main"]:
           isLineNumberAvailable = True
           issueWithLine = newIssue.copy()
           #issueWithLine["external_id"] = issueWithLine["external_id"] + "_" + str(count)
           issueWithLine["path"] = event["strippedFilePathname"]
           issueWithLine["line"] = event["lineNumber"]
           #issueWithLine["details"] = event["eventDescription"]
           count = count + 1
           annotations.append(issueWithLine)

      #print newIssue
      if not isLineNumberAvailable:
        annotations.append(newIssue)

# dump the json
with open('synopsys-bitbucket-sast-report.json', 'w') as fp:
  json.dump(sast_report, fp, indent=4)

with open('synopsys-bitbucket-sast-annotations.json', 'w') as fp:
    json.dump({'annotations': annotations}, fp, indent=4)

print("Created synopsys-bitbucket-sast-report.json and synopsys-bitbucket-sast-annotations.json")
